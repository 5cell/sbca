#!/usr/bin/python

import sys
from json import dumps, loads
from time import sleep
from random import choice

class MyBot:
	def __init__(self, botDetails, otherBots, foods):
		self.botId = botDetails['botId']
		self.snakes = []
		self.otherBots = otherBots
		self.foodsAvailable = foods
		for snake in botDetails['snakes']:
			self.snakes.append(snake)

		self.moves = {}
		self.play()

	def play(self):
		for snake in self.snakes:
			self.moves[snake['snakeId']] = snake['direction']

	def getMoves(self):
		return self.moves

def main():

	sleep(.3)
	data = sys.stdin.readline()

	while data != "quit":
		if data:
			obj = loads(data)
			my_bot = MyBot(obj['myBot'], obj['otherBots'], obj['foodPositions'])
			sys.stdout.write(dumps(my_bot.getMoves())+"\n")
			sys.stdout.flush()
			del my_bot
		data = sys.stdin.readline()



if __name__ == '__main__':
	main()
