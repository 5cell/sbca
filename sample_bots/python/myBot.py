#!/usr/bin/python

import sys
from json import dumps, loads
from time import sleep
from random import choice

xMax = 99
yMax = 99

class MyBot:
	def __init__(self, botDetails, otherBots, foods):
		self.botId = botDetails['botId']
		self.snakes = []
		self.otherBots = otherBots
		self.foodsAvailable = foods
		for snake in botDetails['snakes']:
			self.snakes.append(snake)

		self.moves = {}
		self.play()

	def play(self):
		for snake in self.snakes:
			minDist = 198
			f = None
			for food in self.foodsAvailable:
				if snake['positions'][0][0] <= food[0]:
					xDistance = food[0] - snake['positions'][0][0]
				else:
					xDistance = snake['positions'][0][0] - food[0]
				if snake['positions'][0][1] <= food[1]:
					yDistance = food[1] -snake['positions'][0][1]
				else:
					yDistance = snake['positions'][0][1] - food[1]
				if (xDistance+yDistance) < minDist:
					f = food
					minDist = xDistance+yDistance

			sHead = snake['positions'][0]
			if sHead[0] == f[0]:
				if f[1] < sHead[1]: 
					if snake['direction'] == 4: c = 4
					else: c=3 
				else:
					if snake['direction'] == 3: c = 3
					else: c = 4
			elif sHead[1] == f[1]:
				if f[0] < sHead[0]: 
					if snake['direction'] == 2: c = 2
					else: c=1 
				else:
					if snake['direction'] == 1: c = 1
					else: c = 2
			elif sHead[1] < f[1]:
				if snake['direction'] == 3: c = 3
				else: c= 4
			elif sHead[1] > f[1]:
				if snake['direction'] == 4: c= 4
				else: c = 3 



			self.moves[snake['snakeId']] = c




	def getMoves(self):
		return self.moves

def main():

	sleep(.3)
	data = sys.stdin.readline()

	while data != "quit":
		if data:
			obj = loads(data)
			my_bot = MyBot(obj['myBot'], obj['otherBots'], obj['foodPositions'])
			sys.stdout.write(dumps(my_bot.getMoves())+"\n")
			sys.stdout.flush()
			del my_bot
		data = sys.stdin.readline()



if __name__ == '__main__':
	main()
