#!/usr/bin/python

import sys
from json import dumps, loads
from time import sleep
from random import choice

class MyBot:
	def __init__(self, botDetails, otherBots, foods):
		self.botId = botDetails['botId']
		self.snakes = []
		self.otherBots = otherBots
		self.foodsAvailable = foods
		for snake in botDetails['snakes']:
			self.snakes.append(snake)

		self.moves = {}
		self.play()

	def play(self):
		"""
		    This is the actual function used to write Bot in Python
		    All data structures are initialized those are need to write Bots
		    Data Structures:

		    self.botId : Id of the current Bot
                    self.snakes : Snakes available in the current bot
			you can iterate each snake in self.snakes
			snake['positions'] ==> an array of cordinates of cells
			snake['direction'] ==> direction of snake
			snake['length'] ==> length of snakes
		    self.otherBots : Details of the other Bots
			you can iterate each bot in self.otherBots
			and each bot have multiple snakes, you can itertate through those snake
			snake details can be accessed like above
                    self.foodsAvailable : Currently available foods in the arena(cordinates of foods)
		    

		    next moves should be stored in dictionary 'self.moves' using key snakeId 

		    GOOD LUCK!!!
												"""
		


	def getMoves(self):
		return self.moves

def main():

	sleep(.3)
	data = sys.stdin.readline()

	while data != "quit":
		if data:
			obj = loads(data)
			my_bot = MyBot(obj['myBot'], obj['otherBots'], obj['foodPositions'])
			sys.stdout.write(dumps(my_bot.getMoves())+"\n")
			sys.stdout.flush()
			del my_bot
		data = sys.stdin.readline()



if __name__ == '__main__':
	main()
