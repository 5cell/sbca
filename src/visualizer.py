import pygame
from pygame.locals import *
import time
import sbca_conf

s=pygame.display.set_mode((700, 700))
pygame.display.set_caption('Snake Bot Challenge Arena')

food_image = pygame.Surface((7,7))
food_image.fill((255, 255, 0))

snake_image = pygame.Surface((7,7))

pygame.init()

colors = [(255,0,0),(0,255,0),(0,0,255),(125,125,125)]

bots = {}
clock = pygame.time.Clock()

def visualise( foods, bs ):
	s.fill((0,0,0))
	for food in foods:
		s.blit(food_image,(food[0]*7,food[1]*7))
	for bot in bs:
		for snake in bs[bot]:
			for cord in bs[bot][snake]:
				snake_image.fill(bots[bot])
				s.blit(snake_image,(cord[0]*7,cord[1]*7))
	pygame.display.update()
	clock.tick(10)




def main():
	f = open("bots_file", "r")
	i=0
	for botId in eval(f.readline()):
		bots[botId] = colors[i]
		i = i + 1
	f.close()
	print bots

	i=0
	lines = open(sbca_conf.OUTPUT_FILE_NAME, "r").readlines()
	l = len(lines)
	while i < l:
		bs = eval(lines[i].strip())
		i=i+1
		foods = eval(lines[i].strip())
		i=i+1
		visualise(foods, bs)



if __name__ == '__main__':
	main()
