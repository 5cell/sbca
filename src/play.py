#!/usr/bin/python

from arena import *
import sys
import sbca_conf

def main(botsList, moves, output_file):
	arena = Arena(botsList, moves, output_file)
	if sbca_conf.VISUALIZER:
		import visualizer
		visualizer.main()
	print "Done!!"

if __name__ == '__main__':
	
	if len(sys.argv) < 3:
		print "Please specify atleast 2 Bots"
	elif  len(sys.argv) > 5:
		print "Maximum 4 bots can participate"
	else : main(sys.argv[1:], sbca_conf.MOVES, sbca_conf.OUTPUT_FILE_NAME)
