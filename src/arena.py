
from subprocess import PIPE, Popen
import sys
from random import choice
from json import dumps, loads
from time import sleep


SNAKE_BASIC_LENGTH = 3
MAX_NO_FOOD = 12
INITIAL_SNAKES_NUMBER = 3
MAX_SNAKE_LENGTH = 10

WORLD_BOUNDRY = {
	"X": {"MIN": 0, "MAX": 99},
	"Y": {"MIN": 0, "MAX": 99}
	}


class Snake:

	LEFT = 1
	RIGHT = 2
	UP = 3
	DOWN = 4

	def __init__(self, snakeId, startPosition, direction):
		self.snakeId = snakeId
		self.length = len(startPosition)
		self.direction = direction
		self.cells = startPosition
		boundry = WORLD_BOUNDRY

	def changeDirection(self, newDirection):
		self.direction = newDirection

	def moveSnake(self, foods):
		newCell = self.newCell()
		if newCell in foods:
			self.cells =  [newCell] + self.cells[:]
			self.length = self.length + 1
			return newCell
		else:
			self.cells =  [newCell] + self.cells[:-1]
			return None
		

	def newCell(self):
		if self.direction is Snake.LEFT:
			if self.cells[0][0] == WORLD_BOUNDRY['X']['MIN']:
				return (WORLD_BOUNDRY['X']['MAX'], self.cells[0][1])
			else:
				return (self.cells[0][0]-1, self.cells[0][1])
		if self.direction is Snake.RIGHT:
			if self.cells[0][0] == WORLD_BOUNDRY['X']['MAX']:
				return (WORLD_BOUNDRY['X']['MIN'], self.cells[0][1])
			else:
				return (self.cells[0][0]+1, self.cells[0][1])
		if self.direction is Snake.UP:
			if self.cells[0][1] == WORLD_BOUNDRY['Y']['MIN']:
				return (self.cells[0][0], WORLD_BOUNDRY['Y']['MAX'])
			else:
				return (self.cells[0][0], self.cells[0][1]-1)           
		if self.direction is Snake.DOWN:
			if self.cells[0][1] == WORLD_BOUNDRY['Y']['MAX']:
				return (self.cells[0][0], WORLD_BOUNDRY['Y']['MIN'])
			else:
				return (self.cells[0][0], self.cells[0][1]+1)

	def getCells(self):
		return self.cells

	def getLength(self):
		return self.length

	def splitSnake(self):
		tail = self.cells[4:]
		self.cells =self.cells[:4]
		tail.reverse()
		self.length = len(self.cells)
		if self.direction == 1:
			return {'direction': 2, 'position': tail}
		if self.direction == 2:
			return {'direction': 1, 'position': tail}
		if self.direction == 3:
			return {'direction': 4, 'position': tail}
		if self.direction == 4:
			return {'direction': 3, 'position': tail}

	def getHead(self):
		return self.cells[0]


	def collideOnTail(self):
		if self.cells[0] in self.cells[4:]:
			return True
		else:
			return False

	def collideBySnake(self, snakes):
		for snake in snakes:
			if snake.cells[0] in self.cells:
				return True
		return False

	def getJsonData(self, needDirection):
		jsonObj = {}
		jsonObj['snakeId'] = self.snakeId
		jsonObj['positions'] = self.getCells()
		jsonObj['length'] = self.getLength()
		if needDirection:
			jsonObj['direction'] = self.direction
		return jsonObj
		

class Bot:

	def __init__(self, botName, botId, startPositions, directions):

		self.botName = botName
		self.botId = botId
		self.count = 0
		self.snakes = {}
		self.snakeIdIterator = 1
		self.process = Popen([botName], stdin=PIPE, stdout=PIPE)

		for i in range(1,len(startPositions)+1):
			self.addSnake(startPositions[i], directions[i])

	def addSnake(self, startPosition, direction):
		snake = Snake(self.snakeIdIterator, startPosition, direction)
		self.count = self.count + 1
		snake.direction = direction
		self.snakes[self.snakeIdIterator] = snake
		self.snakeIdIterator = self.snakeIdIterator + 1

	def killSnakes(self, snakes):
		snakeIds = []
		for snake in snakes:
			snakeIds.append(snake.snakeId)
			del self.snakes[snake.snakeId]
			self.count = self.count - 1
		return snakeIds
		

	def deadSnakes(self, opponentSnakes):
		deadSnakes = []
		for snake in self.snakes.values():
			if snake.collideOnTail():
				deadSnakes.append(snake)
				continue
			if snake.collideBySnake(set(self.snakes.values()).difference([snake])):
				deadSnakes.append(snake)
				continue
			if snake.collideBySnake(opponentSnakes):
				deadSnakes.append(snake)
		return deadSnakes

	def getSnakesNumber(self):
		return self.count

	def getCompleteNumberOfCells(self):
		numberOfCells = 0
		for snake in self.snakes.values():
			numberOfCells += snake.getLength()
		return numberOfCells

	def splitSnakes(self):
		newSnakes = []
		for snake in self.snakes.values():
			if snake.getLength() == 8:
				newSnakes.append(snake.splitSnake())
		for snake in newSnakes:
			self.addSnake(snake['position'], snake['direction'])


	def getJsonData(self, needDirection=False):
		jsonObj = {}
		jsonObj['botId'] = self.botId
		jsonObj['snakes'] = self.getSnakesJosn(needDirection)
		return jsonObj

	def sentState(self, data):
		self.process.stdin.write(dumps(data)+"\n")
		self.process.stdin.flush()

	def recieveNextMoves(self):
		data = self.process.stdout.readline().strip()
		print data
		try:
			decoded_data = loads(data)
			return decoded_data
		except :
			print "Error"
			return "Error"

	def getSnakesJosn(self, needDirection):
		return [ snake.getJsonData(needDirection) for snake in self.snakes.values()]


class Arena:

	def __init__(self, bots, maxNoMoves, fileToSave):

		self.botCount = 0
		self.bots = []
		self.numberOfMoves = maxNoMoves
		self.moveCount = 0
		self.file = fileToSave
		f = open(self.file,"w")
		f.close()

		xMin = WORLD_BOUNDRY["X"]["MIN"]
		xMax = WORLD_BOUNDRY["X"]["MAX"]
		yMin = WORLD_BOUNDRY["Y"]["MIN"]
		yMax = WORLD_BOUNDRY["Y"]["MAX"]

		self.cordinates = set([])
		for i in range(xMin,xMax-xMin):
			for j in range(yMin, yMax-yMin):
				self.cordinates.add((i,j))


		startPositions = {}
		startPositions[1] =  {
			1:[(xMin, (yMax+yMin)/2-3), (xMin, (yMax+yMin)/2-2), (xMin, (yMax+yMin)/2-1)],
			2:[(xMin+3,(yMax+yMin)/2), (xMin+2,(yMax+yMin)/2), (xMin+1, (yMax+yMin)/2)],
			3:[(xMin, (yMax+yMin)/2+3), (xMin, (yMax+yMin)/2+2), (xMin, (yMax+yMin)/2+1)]
		}
		startPositions[2] =  {
			1:[(xMax, (yMax+yMin)/2-3), (xMax, (yMax+yMin)/2-2), (xMax, (yMax+yMin)/2-1)],
			2:[(xMax-3,(yMax+yMin)/2), (xMax-2,(yMax+yMin)/2), (xMax-1, (yMax+yMin)/2)],
			3:[(xMax, (yMax+yMin)/2+3), (xMax, (yMax+yMin)/2+2), (xMax, (yMax+yMin)/2+1)]
		}
		startPositions[3] =  {
			1:[((xMin+xMax)/2-3, yMin), ((xMin+xMax)/2-2, yMin), ((xMin+xMax)/2-1, yMin)],
			2:[((xMin+xMax)/2, yMin+3), ((xMin+xMax)/2, yMin+2), ((xMin+xMax)/2, yMin+1)],
			3:[((xMin+xMax)/2+3, yMin), ((xMin+xMax)/2+2, yMin), ((xMin+xMax)/2+1, yMin)]
		}
		startPositions[4] =  {
			1:[((xMin+xMax)/2-3, yMax), ((xMin+xMax)/2-2, yMax), ((xMin+xMax)/2-1, yMax)],
			2:[((xMin+xMax)/2, yMax-3), ((xMin+xMax)/2, yMax-2), ((xMin+xMax)/2, yMax-1)],
			3:[((xMin+xMax)/2+3, yMax), ((xMin+xMax)/2+2, yMax), ((xMin+xMax)/2+1, yMax)]
		}

		directions = {}
		directions [1] = {
			1: Snake.UP,
			2: Snake.RIGHT,
			3: Snake.DOWN
		}
		directions[2] = {
			1: Snake.UP,
			2: Snake.LEFT,
			3: Snake.DOWN
		}
		directions[3] = {
			1: Snake.LEFT,
			2: Snake.DOWN,
			3: Snake.RIGHT
		}
		directions[4] = {
			1: Snake.LEFT,
			2: Snake.UP,
			3: Snake.RIGHT
		}


		bots_file = open("bots_file","w")                   
		bs = []
		for botName in bots:
			self.botCount = self.botCount + 1  
			bs.append(self.botCount)
			bot = Bot(botName, self.botCount, startPositions[self.botCount], directions[self.botCount])
			self.bots.append(bot)
		bots_file.write(str(bs)+"\n")
		bots_file.close()

		snakeCells = []
		for i in startPositions:
			snakeCells = snakeCells + startPositions[i][1] + startPositions[i][2] + startPositions[i][3]
		self.foodHandler = FoodHandler(MAX_NO_FOOD)
		self.foodHandler.generate(self.cordinates.difference(snakeCells))
		self.storeArena()
		self.run()


	def run(self):
		while self.moveCount < self.numberOfMoves:
			print self.moveCount
			#sleep(.2)
			self.sentStateToBots()
			#sleep(.2)
			self.nextMoves()
			self.removeDeadSnakes()
			wonBot = self.updateArena()
			if wonBot:
				print "%s Won the game!!"%wonBot.botName
				return
			self.storeArena()
			self.moveCount = self.moveCount + 1

		if not self.bots:
			print "Every bot failed!!!"
			return
		maxNumberSnakes = []
		for bot in self.bots:
			if not maxNumberSnakes : maxNumberSnakes.append(bot)
			elif maxNumberSnakes[0].getSnakesNumber() == bot.getSnakesNumber():  maxNumberSnakes.append(bot)
			elif maxNumberSnakes[0].getSnakesNumber() < bot.getSnakesNumber(): maxNumberSnakes = [bot]
		if len(maxNumberSnakes) == 1: print "%s Won!!"%maxNumberSnakes[0].botName
		else: 
			wonBots = []
			for bot in maxNumberSnakes:
				if not wonBots: wonBots.append(bot)
				elif wonBots[0].getCompleteNumberOfCells() == bot.getCompleteNumberOfCells(): wonBots.append(bot)
				elif wonBots[0].getCompleteNumberOfCells() < bot.getCompleteNumberOfCells(): wonBots = [bot] 
			print "Bots won: \n"
			for bot in wonBots:
				print "%s"%bot.botName


	def storeArena(self):
		with open(self.file, "a") as f:
			line = {}
			for bot in self.bots:
				line[bot.botId] = {}
				for snake in bot.snakes.values():
					line[bot.botId][snake.snakeId] = snake.getCells()
			f.write(str(line)+"\n")
			f.flush()
			line = self.foodHandler.getFoods()
			f.write(str(line)+"\n")
			f.flush()


	def nextMoves(self):
		foods = self.foodHandler.getFoods()
		for bot in self.bots:
			moves = bot.recieveNextMoves()
			print moves
			for snake in bot.snakes.values():
				snake.changeDirection(moves[str(snake.snakeId)])
				food = snake.moveSnake(foods)
				if food is not None: self.foodHandler.remove(food)

	def removeDeadSnakes(self):
		deadSnakes = {}
		for bot in self.bots:
			opponentSnakes = []
			for oppBot in self.bots:
				if oppBot is not bot:
					opponentSnakes = opponentSnakes + oppBot.snakes.values()
			deadSnakes[bot] = bot.deadSnakes(opponentSnakes)
		deadSnakesIds = []
		for bot in deadSnakes:
			deadSnakesIds = deadSnakesIds + bot.killSnakes(deadSnakes[bot])
		return deadSnakesIds    

	
	def updateArena(self):
		cordinatesWithSnakes = []
		for bot in self.bots:
			if bot.getSnakesNumber() == 0:
				self.bots.remove(bot)
			else:
				bot.splitSnakes()
				for snake in bot.snakes.values():
					cordinatesWithSnakes = cordinatesWithSnakes + snake.getCells()
		if len(self.bots) == 1 : return self.bots[0]
		self.foodHandler.generate(self.cordinates.difference(cordinatesWithSnakes))
		return None

	def getBotJsonData(self, bot):
		jsonObj = {}
		jsonObj['otherBots'] = []
		jsonObj['myBot'] = bot.getJsonData(needDirection=True)
		for b in self.bots:
			if b is not bot:
				jsonObj['otherBots'].append(b.getJsonData())
		jsonObj['foodPositions'] = self.foodHandler.getFoods()
		jsonObj['directions'] = {Snake.LEFT:"left", Snake.RIGHT:"right", Snake.UP: "up", Snake.DOWN: "down"}
		jsonObj['boundry'] = WORLD_BOUNDRY
		return jsonObj

	def sentStateToBots(self):
		for bot in self.bots:
			bot.sentState(self.getBotJsonData(bot))


	def finish(self, x):
		pass
		

class FoodHandler:

	def __init__(self, maxNo):
		self.maxNo = maxNo
		self.foods = []
		

	def generate(self, availablePositions):
		availablePositions.difference_update(self.foods)
		noFood = self.maxNo - len(self.foods)
		while noFood > 0:
			position = choice(list(availablePositions))
			self.foods.append(position)
			noFood = noFood - 1
		return self.foods

	def remove(self, food):
		self.foods.remove(food)

	def getFoods(self):
		return self.foods
